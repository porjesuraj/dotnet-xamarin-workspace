﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingletonDemo_Web.Managers
{
   public interface iEmployeeManager
    {

        decimal GetBonus();

        decimal GetPay(); 
    }
}
