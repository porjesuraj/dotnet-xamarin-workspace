﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace layout.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AbsolutePageExercise : ContentPage
    {
        public AbsolutePageExercise()
        {
            InitializeComponent();
        }
    }
}