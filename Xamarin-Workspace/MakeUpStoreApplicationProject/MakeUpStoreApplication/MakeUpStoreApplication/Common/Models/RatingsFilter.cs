﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MakeUpStoreApplication.Common.Models
{
   public class RatingsFilter
    {
        public int RatingGreaterThan { get; set; }

        public string RatingInfo { get; set; }

    }
}
