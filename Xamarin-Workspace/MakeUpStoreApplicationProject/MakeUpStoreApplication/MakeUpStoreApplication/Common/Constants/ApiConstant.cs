﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MakeUpStoreApplication.Common.Constants
{
   public static class ApiConstant
    {
        public static readonly string API_BASE = "https://makeup-api.herokuapp.com/api/v1/products.json";
    }
}
